# delightful databases [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/master/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A curated list of delightful databases in all shapes and sizes, and all FOSS.

<!-- Everyone is invited to contribute. To do so, please read guidelines at: https://codeberg.org/teaserbot-labs/delightful -->

## Table of contents

- [Relational databases](#relational-databases)
- [Key-value databases](#key-value-databases)
- [Document-oriented databases](#document-oriented-databases)
- [Graph databases](#graph-databases)
- [Time series databases](#time-series-databases)
- [Multi-modal databases](#multi-modal-databases)
- [Maintainers](#maintainers)
- [Contributors](#contributors)
- [License](#license)

## Relational databases

| Website | Summary | License |
| :---: | --- | --- |
| [Cassandra](https://gitbox.apache.org/repos/asf?p=cassandra.git) | Free and open-source, distributed, wide column store, NoSQL database management system. | Apache License 2.0 |
| [MariaDB](https://github.com/MariaDB/server) | Community developed fork of MySQL server. | GPLv2 |
| [PostgreSQL](https://git.postgresql.org/gitweb/?p=postgresql.git) | Powerful, open source object-relational database system. | PostgreSQL Licence |
| [SQLite](https://sqlite.org/src) | Small, fast, self-contained, high-reliability, full-featured, SQL database engine. | Public domain |
| [TiDB](https://github.com/pingcap/tidb) | An open source distributed HTAP database compatible with the MySQL protocol. | Apache License 2.0 |

## Key-value databases

| Website | Summary | License |
| :---: | --- | --- |
| [KeyDB](https://github.com/JohnSully/KeyDB) | Multithreaded Fork of Redis. | BSD 3-Clause |
| [Redis](https://github.com/antirez/redis) | Open source, in-memory data structure store, used as a database, cache and message broker. | BSD 3-Clause |
| [AntidoteDB](https://github.com/AntidoteDB/antidote) | A planet scale, highly available, transactional database built on CRDT technology. | Apache License 2.0 |
| [Graviton](https://github.com/deroproject/graviton) | A simple, fast, versioned, authenticated, embeddable key-value store database in pure Golang. | GPLv3 |

## Document-oriented databases

| Website | Summary | License |
| :---: | --- | --- |
| [CouchDB](https://github.com/apache/couchdb) | Open source NoSQL document database that stores data in JSON documents accessible via HTTP. | Apache License 2.0  |
| [Earthstar](https://github.com/cinnamon-bun/earthstar) | Offline-first, distributed, syncable, embedded document database for use in p2p software. | AGPL 3.0  |
| [MongoDB](https://github.com/mongodb/mongo) | General purpose, document-based, distributed database built for modern application developers and for the cloud era. | Server Side Public License  |
| [RethinkDB](https://github.com/rethinkdb/rethinkdb) | Distributed, highly available, open-source database that stores schemaless JSON documents. | Apache License 2.0  |

## Graph databases

| Website | Summary | License |
| :---: | --- | --- |
| [Blazegraph](https://github.com/blazegraph/database) | High-performance graph database supporting Blueprints and RDF/SPARQL APIs. | GPLv2 |
| [BrightstartDB](https://github.com/BrightstarDB/BrightstarDB) | A native .NET RDF triple store that uses LINQ for querying. | MIT |
| [Cayley](https://github.com/cayleygraph/cayley) | Open source database for Linked Data. It is inspired by the graph database behind Google's Knowledge Graph (formerly Freebase). | Apache License 2.0 |
| [Dgraph](https://github.com/dgraph-io/dgraph) | Fast, transactional, distributed Graph Database with support for GraphQL-like query syntax. | Apache License 2.0 |
| [Fortune.js](https://github.com/fortunejs/fortune) | Non-native graph database abstraction layer for Node.js and web browsers. | MIT |
| [Gaffer](https://github.com/gchq/Gaffer) | A large-scale entity and relation database supporting aggregation of properties. | Apache License 2.0 |
| [Grakn.ai](https://github.com/graknlabs/grakn) | An intelligent database: a knowledge graph engine to organise complex networks of data and make it queryable. | AGPLv3 |
| [Gun](https://github.com/amark/gun) | A realtime, decentralized, offline-first, graph protocol to sync the web. | Apache License 2.0 |
| [JanusGraph](https://github.com/JanusGraph/janusgraph) | A highly scalable graph database optimized for storing and querying large graphs with billions of vertices and edges. | Apache License 2.0 / CC-BY-4.0 |
| [NebulaGraph](https://github.com/vesoft-inc/nebula) | A distributed, fast open-source graph database featuring horizontal scalability and high availability. | Apache License 2.0 |
| [Neo4j](https://github.com/neo4j/neo4j) | Highly scalable native graph database, purpose-built to leverage not only data but also data relationships. | GPLv3 |
| [RecallGraph](https://github.com/RecallGraph/RecallGraph) | A versioning data store for time-variant graph data. | Apache License 2.0 |
| [TerminusDB](https://github.com/terminusdb/terminusdb-server) | A model driven in-memory graph database designed for the web-age using JSON-LD exchange format. | GPLv3 |

## Time series databases

| Website | Summary | License |
| :---: | --- | --- |
| [InfluxDB](https://github.com/influxdata/influxdb) | Scalable datastore for metrics, events, and real-time analytics. | MIT |
| [TimescaleDB](https://github.com/timescale/timescaledb) | Open-source database built for analyzing time-series data with the power and convenience of SQL. | Apache License 2.0 |

## Multi-modal databases

| Website | Summary | License |
| :---: | --- | --- |
| [ArangoDB](https://github.com/arangodb/arangodb) | A native multi-model database with flexible data models for documents, graphs, and key-values. | Apache License 2.0 |
| [OrbitDB](https://github.com/orbitdb/orbit-db) | A serverless, distributed, peer-to-peer database based on IPFS | MIT |
| [OrientDB](https://github.com/orientechnologies/orientdb) | The most versatile DBMS supporting Graph, Document, Reactive, Full-Text, Geospatial and Key-Value models. | Apache License 2.0 |
| [MartenDB](https://github.com/JasperFx/Marten) | A .NET transactional Document DB and Event Store on top of PostgreSQL. | MIT |

## Maintainers

If you have questions or feedback regarding this list, then please create
an [Issue](https://codeberg.org/yarmo/delightful-databases/issues) in our tracker, and optionally `@mention` one or more of our maintainers:

- [`@yarmo`](https://codeberg.org/yarmo)

## Contributors

With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](https://codeberg.org/teaserbot-labs/delightful/src/branch/master/delight-us.md#attribution-of-contributors) if you are missing).

## License

[![CC0 Public domain. This work is free of known copyright restrictions.](https://i.creativecommons.org/p/mark/1.0/88x31.png)](https://creativecommons.org/publicdomain/zero/1.0/)